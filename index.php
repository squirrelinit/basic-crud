<?php include 'Contato.php';

$contato = new Contato();

$info = $contato->read();

?>
<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <title>Crud</title>
</head>
<body>

<div class="container text-center">
    <div class="card">

        <div class="card-header">
            <h2>Contatos</h2>
        </div>
        <a class="text-left" href="adicionar.php"><button style="margin-top: 10px; margin-left: 15px" class="btn btn-info">Adicionar</button></a>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                      <th>Id</th>
                      <th>Nome</th>
                      <th>E-mail</th>
                      <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                foreach ($info as $item):
                ?>
                    <tr>
                        <td><?php echo $item['id']; ?></td>
                        <td><?php echo $item['nome']; ?></td>
                        <td><?php echo $item['email']; ?></td>
                        <td>
                            <a href="editar.php?id=<?php echo $item['id'];?>"><button class="btn btn-success btn-sm">Editar</button></a>
                            <a href="excluir.php?id=<?php echo $item['id']; ?>"><button class="btn btn-danger btn-sm">Excluir</button></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>




<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>
</html>