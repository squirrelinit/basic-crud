<?php
include 'Contato.php';

$contato = new Contato();
$excluir = $contato->delete($_GET['id']);

if($excluir === true)
{
	header("Location: index.php");
}else
{
	echo '<div class="alert alert-danger fade show" role="alert">
				Houve erro na Exclusão
				<button type="button" class="close" data-dismiss="alert" aria-label="close">
				<span>&times;</span>
				</button>
			</div>
			';
}
?>
<script>
	$(function(){
		setTimeout(function(){
			$('.alert').alert('close');
		}, 2000)
	})
</script>
