<?php
include 'Contato.php';

$contato = new Contato();
$info = $contato->read();

?>
<!DOCTYPE html>
<html lang="pt">
<head>
	<meta charset="utf-8" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	<title>Crud</title>
</head>
<body>

<div class="container text-center">
	<div class="card">

		<div class="card-header">
			<h2>Editar contatos</h2>
		</div>

		<a class="text-left" style="margin-left: 8px;" href="index.php"><button style="margin-top: 10px; margin-left: 15px" class="btn btn-danger btn-sm">Cancelar</button></a>

		<div class="card-body">

			<form method="post" class="text-left">
				<?php

				if(isset($_POST['acao']) && !empty($_POST['acao']))
				{
					$nome = $_POST['nome'];
					$email = $_POST['email'];
					$id = $_GET['id'];

					$editar = $contato->update($nome, $email, $id);
					if($editar == true)
					{
						header("Location: index.php");
					}else
					{
						echo '<div class="alert alert-danger fade show" role="alert">
								Houve erro na edição
								<button type="button" class="close" data-dismiss="alert" aria-label="close">
								<span>&times;</span>
								</button>
							</div>';
					}


				}
				?>
                <?php

                    foreach ($info as $info):
                        if($info['id'] == $_GET['id']):
                ?>
				<input style="margin-bottom: 15px;" class="form-control w-50" type="text" name="nome" value="<?php echo $info['nome']; ?>" placeholder="Nome" />
				<input style="margin-bottom: 15px;" class="form-control w-50" type="email" name="email" value="<?php echo $info['email']; ?>" placeholder="E-mail"/>
                <?php
                        endif;
                     endforeach;
                ?>
				<input class="btn btn-success btn-sm" type="submit" name="acao" value="Salvar" />
			</form>

		</div><!--card-body-->
	</div><!--card-->
</div><!--container-->




<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<script>
	$(function(){
		setTimeout(function(){
			$('.alert').alert('close');
		}, 2000)
	})
</script>
</body>
</html>