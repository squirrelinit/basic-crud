<?php



class Contato
{
    public function __construct()
    {
        $this->pdo = new PDO("mysql:host=localhost;dbname=crudoo",'root', 'mysql');
    }

    //Função Criar
    public function create($nome, $email)
    {
        if($this->existInfo($nome, $email))
        {
            $sql = "INSERT INTO contatos (nome, email) VALUES (:nome, :email)";
            $sql = $this->pdo->prepare($sql);
            $sql->bindValue(':nome', $nome);
            $sql->bindValue(':email', $email);
            $sql->execute();

            if($sql->rowCount() > 0)
            {
                return true;
            }else
            {
                return false;
            }
        }else
        {
            return false;
        }
    }
    //Função Ler
    public function read()
    {
            $sql = "SELECT * FROM contatos";
            $sql = $this->pdo->query($sql);
            $sql->execute();

            if($sql->rowCount() > 0)
            {

	            return $sql->fetchAll();

            }else
            {
                return array();
            }

    }

    //Função Editar
	public function update($nome, $email, $id)
	{
        if($this->existInfo($nome, $email))
        {
        	$sql = "UPDATE contatos SET nome = :nome, email = :email WHERE id = :id";
        	$sql = $this->pdo->prepare($sql);
        	$sql->bindValue(':nome', $nome);
        	$sql->bindValue(':email',$email);
        	$sql->bindValue(":id", $id);
        	$sql->execute();

        	return true;
        }else
        {
        	return false;
        }
	}

    //Função Excluir
    public function delete($id)
    {
        $sql = "DELETE FROM contatos WHERE id = :id";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(':id', $id);
        $sql->execute();

        if($sql->rowCount() > 0)
        {
        	return true;
        }else
        {
        	return false;
        }
    }

    private function existInfo($nome, $email)
    {
        $sql = "SELECT nome, email FROM contatos WHERE nome = :nome AND email = :email";
        $sql = $this->pdo->prepare($sql);
        $sql->bindValue(':nome', $nome);
        $sql->bindValue(':email', $email);
        $sql->execute();

        if($sql->rowCount() > 0)
        {
            return false;
        }else
        {
            return true;
        }
    }
}